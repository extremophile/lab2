#include <iostream>
#include <string>
using namespace std;

int main()
{
    cout << "Enter your first name: " << endl;
    string firstName;
    cin >> firstName;
    cout << "Enter your last name: " << endl;
    string lastName;
    cin >> lastName;

    string adress;
    cout << "Enter your adress: " << endl;
    cin.ignore();
    getline(cin,adress);


    std::cout << std::string(18, '=') << std::endl;
    std::cout << std::string(18, ' ') << std::endl;
    cout << "> " << firstName << endl;
    std::cout << std::string(18, ' ') << std::endl;
    cout << "> " << lastName << endl;
    std::cout << std::string(18, ' ') << std::endl;
    cout << "> " << adress << endl;
    std::cout << std::string(18, ' ') << std::endl;
    std::cout << std::string(18, '=') << std::endl;
    }

