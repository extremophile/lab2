#include <iostream>
#include <string>

using namespace std;

int main()
{
    cout << "Enter your name: " << endl;
    string firstName;
    cin >> firstName;

    std::cout << std::string(16, '=') << std::endl;
    std::cout << std::string(16, ' ') << std::endl;
    cout << "=" << std::string(5, ' ') <<  firstName << std::string(5, ' ') << "=" << endl;
    std::cout << std::string(16, ' ') << std::endl;
    std::cout << std::string(16, '=') << std::endl;
    }
